import org.apache.spark.sql.functions._
import org.apache.spark.sql.SparkSession
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.types._
import org.apache.spark.sql._
import org.apache.spark.SparkContext
import org.elasticsearch.spark._
import org.elasticsearch.spark.sql._
import java.util.Formatter.DateTime
import org.apache.spark.SparkConf

object ETLMobilidade {

  def main(args: Array[String]){
    
    System.out.println("Iniciando")
    
    if (args.length < 2) {
      System.err.println("Parametros de entrada: <mascara de busca de arquivos> <nome do indice do elasticsearch>")
      System.exit(1)
    }
    
    var mascaraAquivos = args(0)
    var indiceES = args(1)+"/doc"
    
    
    if(mascaraAquivos != "*")
      mascaraAquivos+="*"
      
    //criando sessão do spark
    val spark = SparkSession
      .builder
      .master("spark://UbuntuVM.yffeowwlbxdeblzbjw3sx2fn4b.nx.internal.cloudapp.net:7077")
      .appName("Extracao")
      .getOrCreate()


    //configuração do ElasticSearch
    val conf = new SparkConf().setAppName("WriteToES")
    
    var configES: Map[String,String] = Map()
    configES += ("es.nodes" -> "tebdi.brazilsouth.cloudapp.azure.com") 
    configES += ("es.mapping.id" -> "id")
    configES += ("es.index.auto.create" -> "true")    
    
    //variáveis de contexto do spark
    val sc = SparkContext.getOrCreate(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
  

    //carregando os arquivos legados
    val etl1 = sqlContext.read.json("hdfs://tebdi-cluster:9000/home/user/medicoes/"+mascaraAquivos)

    //executando transposição do array de dados
    val etl2 = etl1.withColumn("data", explode( col("DATA")))
    
    //convertendo cada item do array para colunas
    val tabela = etl2.withColumn("datahora",   col("data")(0))
                     .withColumn("ordem",      col("data")(1))
                     .withColumn("linha",      col("data")(2))
                     .withColumn("lat",        col("data")(3))
                     .withColumn("lng",        col("data")(4))
                     .withColumn("velocidade", col("data")(5))

    //criando view "leituras"
    tabela.createOrReplaceTempView("leituras")

    //saneando e formatando dados da tabela
    val tabela_final = sqlContext.sql("select distinct datahora, ordem, linha, lat || ',' || lng as localizacao, velocidade from leituras")
           .withColumn("linha", regexp_replace(
                                   translate(when(col("linha").isNull ,0).otherwise(col("linha")),".0","")
                                   ,"[^0-9]+",""
                                )
                      )
           .withColumn("id", translate(translate(translate(concat(col("datahora") ,col("ordem"))," ",""),":",""),"-",""))
           
    //removendo dados sem data/hora
    tabela_final.filter(col("datahora").isNotNull)
  
    //exportando dados para o Elastic Search
    tabela_final.saveToEs(indiceES,configES)
    
  }
}


