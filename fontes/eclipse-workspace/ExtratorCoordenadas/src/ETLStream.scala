import org.apache.spark.sql.functions._
import org.apache.spark.sql.SparkSession
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.types._
import org.apache.spark.sql._
import org.apache.spark.SparkContext
import org.elasticsearch.spark._
import org.elasticsearch.spark.sql._
import java.util.Formatter.DateTime
import java.util.Formatter.DateTime
import org.apache.spark.SparkConf

object ETLStream {

  def main(args: Array[String]){
    
  val spark =  SparkSession.builder()
  //.master("spark://UbuntuVM.yffeowwlbxdeblzbjw3sx2fn4b.nx.internal.cloudapp.net:7077")
  .config("es.mapping.id", "id")
  .appName("ExtracaoStreaming")           
  .getOrCreate()
       


   val conf = new SparkConf().setAppName("WriteToESStream")
  
    var configES: Map[String,String] = Map()
    configES += ("es.nodes" -> "tebdi.brazilsouth.cloudapp.azure.com") 
    configES += ("es.mapping.id" -> "id")
    
    val sc = SparkContext.getOrCreate(conf)
   
    //val sqlContext = new org.apache.spark.sql.SQLContext(sc)
  
    val userSchema = new StructType().add("datahora", "string")
                                .add("ordem", "string")
                                .add("linha", "string")
                                .add("latitude", "string")
                                .add("longitude", "string")
                                .add("velocidade", "integer")
 
                                 
   val medicoes = spark.readStream  
  .option("sep", ",")
  .schema(userSchema)
   .csv("/home/marcos/medicoesstream")
   
   
   val df = medicoes
           .withColumn("localizacao", concat(col("latitude") ,lit(","), col("longitude")))
           .withColumn("linha", regexp_replace(
                                   translate(when(col("linha").isNull ,0).otherwise(col("linha")),".0","")
                                   ,"[^0-9]+",""
                                ))
           .withColumn("id", translate(translate(translate(concat(col("datahora") ,col("ordem"))," ",""),":",""),"-",""))
           
           .filter(col("datahora").isNotNull)
                   

    /*val query = df.writeStream
      .outputMode("append")            
      .format("console")
      .start() 
   */
        
    val query = df.writeStream
      .outputMode("append")
      .option("checkpointLocation", "/home/marcos/streamcheckpoint")      
      .format("es")
      .start("medicaostream/doc") 
      
    query.awaitTermination()
  }
}
